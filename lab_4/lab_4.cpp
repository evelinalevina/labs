﻿#include <iostream>
#include <chrono> 
#include <vector>

struct T_List
{
	T_List* next;
	int age;
};

void ADD(T_List* head, int age)
{
	T_List* p = new T_List;
	p->age = age;
	p->next = head->next;
	head->next = p;
}

void PRINT(T_List* head)
{
	T_List* p = head->next;
	while (p != nullptr)
	{
		std::cout << p->age << std::endl;
		p = p->next;
	}
}

void CLEAR(T_List* head)
{
	T_List* tmp;
	T_List* p = head->next;

	while (p != nullptr)
	{
		tmp = p;
		p = p->next;
		delete tmp;
	}
}

void DELETE(T_List* head, int& k)
{
	T_List* tmp;
	T_List* p = head;
	while (p->next != nullptr)
	{
		if (p->next->age == k)
		{
			tmp = p->next;
			p->next = p->next->next;
			delete tmp;
		}
		else
			p = p->next;
	}
}


class Timer
{
private:
	using clock_t = std::chrono::high_resolution_clock;
	using second_t = std::chrono::duration<double, std::ratio<1> >;

	std::chrono::time_point<clock_t> m_beg;

public:
	Timer() : m_beg(clock_t::now())
	{
	}

	void reset()
	{
		m_beg = clock_t::now();
	}

	double elapsed() const
	{
		return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
	}
};

int main()
{
	int N = 10000;
	int M = 1000;
	srand(time(NULL));

	std::vector<int> vec;
	for (int i = 0; i <= N; i++)
		vec.push_back(rand() % 1000);

	T_List* head = new T_List;
	head->next = nullptr;
	for (int i = 0; i <= N; i++)
		ADD(head, rand() % 1000);

	int k= rand() % 1000;
	std::cout << "Delete all numbers "<< k << std::endl;

	/*for (int i = 0; i <= N; i++)
		std::cout << array[i] << std::endl;*/


	int count = 0;
	Timer vector;
	for (int i = 0; i < vec.size(); i++)
	{
		if (vec[i] == k)
		{
			vec.erase(vec.begin() + i);
			count += 1;
			
		}
	}
	std::cout << "Time(vector): " << vector.elapsed() << '\n';
	/*for (int i = 0; i <= N; i++)
		std::cout << array[i] << std::endl;*/


	//PRINT(head);
	Timer list;
	for (int i = 0; i < N; i++)
		DELETE(head, k);
	std::cout << "Time(list): " << list.elapsed() << '\n';
	//PRINT(head);

	if (count > 0)
		std::cout << count << " numbers were deleted." << std::endl;
	else
		std::cout << "Number was not found!" << std::endl;
	
	
	CLEAR(head);
	delete head;
	return 0;
}


