﻿#include <iostream>
#include <fstream>


struct Elem
{
    int data;         
    Elem* left;
    Elem* right;
    Elem* parent;
};


Elem* MAKE(int data, Elem* p)
{
    Elem* q = new Elem;      
    q->data = data;
    q->left = nullptr;
    q->right = nullptr;
    q->parent = p;
    return q;
}



void PASS(Elem* v)
{
    if (v == nullptr)
        return;
    PASS(v->left);
    std::cout << v->data << std::endl;
    PASS(v->right);

}

Elem* SEARCH(int data, Elem* v)
{
    if (v == nullptr)
        return v;
    if (v->data == data)
        return v;
    if (data < v->data)
        return SEARCH(data, v->left);
    else
        return SEARCH(data, v->right);
}

void ADD(int data, Elem*& root)
{
    if (root == nullptr) {
        root = MAKE(data, nullptr);
        return;
    }
    Elem* v = root;
    while ((data < v->data && v->left != nullptr) || (data > v->data && v->right != nullptr))
        if (data < v->data)
            v = v->left;
        else
            v = v->right;
    if (data == v->data)
        return;
    Elem* u = MAKE(data, v);
    if (data < v->data)
        v->left = u;
    else
        v->right = u;
}


void CLEAR(Elem*& v)
{
    if (v == nullptr)
        return;
    CLEAR(v->left);
    CLEAR(v->right);
    delete v;
    v = nullptr;
}


int main()
{
    Elem* root = nullptr;
    int k, n, l=0;
    std::cin >> k;
    std::ifstream in("input.txt");
    while (!in.eof())
    {
        in >> n;
        ADD(n, root);
        if (n == 0)
            CLEAR(root);

        if ((SEARCH(n, root) != 0) && (n == k))
            l += 1;
    }
    
    if (l > 0)
        std::cout << "found "<< l << std::endl;
    else
        std::cout << "not found" << std::endl;
    CLEAR(root);
    
    return 0;
}