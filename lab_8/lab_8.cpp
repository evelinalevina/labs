﻿#include <iostream>
#include <cassert>
#include "inc/matrix.hpp"


using el::Mat22d;
using el::Vec2d;

int main()
{
    std::cout << "=== Test 1 ===" << std::endl;
    {
        Mat22d A({ {
                 {1,2},
                 {3,4}
        } });

        Vec2d X({ {
                {1},
                {1}
         } });

        auto B = A * X;

        assert(B.get(0, 0) == 3);
        assert(B.get(1, 0) == 7);
    }
    std::cout << "Done!" << std::endl;

    std::cout << "=== Test 2 ===" << std::endl;
    {
        Mat22d A({ {
             {1,2},
             {3,4}
        } });

        Mat22d X({ {
            {1,1},
            {1,1}
        } });

        auto B = A + X;
        assert(B.get(0, 0) == 2);
        assert(B.get(0, 1) == 3);
        assert(B.get(1, 0) == 4);
        assert(B.get(1, 1) == 5);
    }
    std::cout << "Done!" << std::endl;

    std::cout << "=== Test 3 ===" << std::endl;
    {
        Mat22d A({ {
             {1,2},
             {3,4}
        } });

        Mat22d X({ {
            {1,1},
            {1,1}
        } });

        auto B = A - X;
        assert(B.get(0, 0) == 0);
        assert(B.get(0, 1) == 1);
        assert(B.get(1, 0) == 2);
        assert(B.get(1, 1) == 3);
    }
    std::cout << "Done!" << std::endl;


    std::cout << "=== Test 4  ===" << std::endl;
    {
        Mat22d A({ {
                 {1,2},
                 {3,4}
         } });

        auto B = A.Det();
        assert(B == -2);
    }
    std::cout << "Done!" << std::endl;


    std::cout << "=== Test 5 ===" << std::endl;
    {
        Mat22d A({ {
                 {1,2},
                 {3,4}
         } });

        auto B = A.Trans();
        assert(B.get(0, 0) == 1);
        assert(B.get(0, 1) == 3);
        assert(B.get(1, 0) == 2);
        assert(B.get(1, 1) == 4);

    }
    std::cout << "Done!" << std::endl;


    std::cout << "=== Test 6 ===" << std::endl;
    {

        try
        {
            Mat22d A({ {
                {1,2},
                {3,4}
        } });

            auto B = A.Inv();
        }

        catch (const std::exception& e)
        {
            std::cerr << e.what() << std::endl;
        }

    }
    std::cout << "Done!" << std::endl;



    return 0;
}