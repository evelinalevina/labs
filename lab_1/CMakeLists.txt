cmake_minimum_required(VERSION 3.20.2)
project(lab_1)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_EXTENSIONS OFF)
set(CMAKE_CXX_STANDARD_REQUIRED TRUE)


add_executable(lab_1 lab_1.cpp)