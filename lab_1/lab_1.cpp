﻿#include <iostream>
#include <chrono> 

class Timer
{
private:
	using clock_t = std::chrono::high_resolution_clock;
	using second_t = std::chrono::duration<double, std::ratio<1> >;

	std::chrono::time_point<clock_t> m_beg;

public:
	Timer() : m_beg(clock_t::now())
	{
	}

	void reset()
	{
		m_beg = clock_t::now();
	}

	double elapsed() const
	{
		return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
	}
};


bool isPrime(int x)
{
    if (x < 2)
        return false;
    for (int d = 2; d <= sqrt(x); d++)
        if (x % d == 0)
            return false;
    return true;
}

int main()
{
    srand(time(0));
    int n, k=0;
    int arr[10000];
    n = rand()%10000;
    for (int i = 0; i < n; i++) 
        arr[i] = rand() % 10000;
    Timer t;
    for (int i = 0; i < n; i++)
    {
        if (isPrime(arr[i]))
            k++;
    }
    std::cout << k << std::endl;;
    std::cout << "Time elapsed: " << t.elapsed() << '\n';

    return 0;
}

