﻿#include <iostream>
#include <chrono>
#pragma comment(linker, "/STACK:4000000000")
#define N 100000


class Timer
{
private:
	using clock_t = std::chrono::high_resolution_clock;
	using second_t = std::chrono::duration<double, std::ratio<1> >;

	std::chrono::time_point<clock_t> m_beg;

public:
	Timer() : m_beg(clock_t::now())
	{
	}

	void reset()
	{
		m_beg = clock_t::now();
	}

	double elapsed() const
	{
		return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
	}
};

void quickSort(int a, int b, int arr[N])
{
	if (a >= b)
		return;
	int m = ((rand() * rand()) % (b - a + 1)) + a;
	int k = arr[m];
	int l = a - 1;
	int r = b + 1;
	while (1)
	{
		do l = l + 1; while (arr[l] < k);
		do r = r - 1; while (arr[r] > k);
		if (l >= r)
			break;
		std::swap(arr[l], arr[r]);
		r = l;
		l--;
		quickSort(a, l, arr);
		quickSort(r, b, arr);

	}

}

void bubleSort(int arr[N], int n)
{
	for (int i = 1; i < n; i++)
	{
		if (arr[i] >= arr[i - 1])
			continue;
		int j = i - 1;
		while (j >= 0 && arr[j] > arr[j + 1])
		{
			std::swap(arr[j], arr[j + 1]);
			j--;
		}
	}
}


int main()
{
	srand(time(0));
	int arr1[N];
	int arr2[N];
	for (int i = 0; i < N; i++)
		arr1[i] = rand();
	for (int i = 0; i < N; i++)
		arr2[i] = arr1[i];
	int a = 0;
	int b = N - 1;

	Timer t1;
	quickSort(a, b, arr1);
	std::cout << "Time taken: " << t1.elapsed() << '\n';

	Timer t2;
	bubleSort(arr2, N);
	std::cout << "Time taken: " << t2.elapsed() << '\n';

	/*for (int i = 0; i < N; i++)
		std::cout << arr1[i] << std::endl;
	for (int i = 0; i < N; i++)
		std::cout << arr2[i] << std::endl;*/
}

